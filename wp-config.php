<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'meris');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[^i;qTwKT&]kF9]Bm=cv1k.sgk8#+)>9BK,ft>CaP#LTne.4OsaY]CiWfXF;tc!c');
define('SECURE_AUTH_KEY',  't94JK+/jq&w)rx7^x5Qq3zAD%Y7whZWO|rwp;}jq(ca{bwcRR|KqpdrSY`%A 9l?');
define('LOGGED_IN_KEY',    '4P;NWQCGfNn7F1Qkr:`R#4V!EN<Z+y:p.Gc;kVh].!3/tt;(n=&]CJ_2?5p}v3@F');
define('NONCE_KEY',        'x4|ZdbMFj&s-(53Z_e~ yd;Cc]9|CgxwV0|6<x$@H*T<$u(fa`dRn.E!42s6!Q?9');
define('AUTH_SALT',        '}j2E0RKnPQC-F?G3vHsG15]t9T6Bm=T/<~$O9bBj+L*1dcG}7Flq6K]&XaY)CV62');
define('SECURE_AUTH_SALT', '<(u(IEuaP`A5z32^%m`J8BHf/VaGH4RAzQ7.fI]UFr?[pTvPEBf{*5Ih7xi4XE!`');
define('LOGGED_IN_SALT',   '}OSg[+$^DB$*J &o?x.,9u+{#X(oTUX>NoqI3N@n<1u:j%Kk6X/d.ln,|ox>REq|');
define('NONCE_SALT',       'mzHR7o7R8GjK],;gsU7p[MWZ /j=<2NNQD]rr+GS!9di^;]lj0NxWTQigUoXxE5x');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_hive_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
