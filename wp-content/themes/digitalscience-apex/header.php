<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title><?php wp_title(); ?></title>
<?php apex_favicon(); ?>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<?php
	$settings = apex_section_settings();
	if( $settings['page_top'] == 'header' ) {
		get_template_part( 'templates/elements/header' );
	} elseif( $settings['page_top'] == 'hero' ) {
		get_template_part( 'templates/elements/hero' );
	}
	?>
	
	<?php global $apex_general_settings; ?>

	<header id="site-navigation" class="apex-style-<?php echo $apex_general_settings['navigation_style']; ?>">
    <?php echo get_apex_menu_logo(); ?>

    <div id="site-navigation-contents">
      <div class="container">
	
				
				
				<a id="mobile-menu-toggle" href="#"><i class="apex-icon-mobile-menu"></i></a>
	
				<?php
				get_search_form();
				$args = array(
					'theme_location'		=> 'apex-primary-menu',
					'menu_class'				=> 'apex-primary-menu clearfix',
					'container'					=> 'nav',
					'container_class'		=> 'apex-primary-menu-container',
					'fallback_cb'				=> 'primary_menu_fallback',
					'walker'  					=> new Apex_Icon_Menu_Walker()
				);
				wp_nav_menu( $args );
				
				

				?>

	
			</div><!-- .container -->
    </div><!-- #site-navigation-contents -->
  </header><!-- #site-navigation -->
  
  <div id="wrapper">

		<div id="main" role="main">