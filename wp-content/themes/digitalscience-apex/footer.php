		<?php
		get_template_part( 'templates/elements/extra', 'content' );
		
		global $apex_general_settings; ?>
		
		</div><!-- #main -->
	
		<footer id="site-footer" class="apex-style-<?php echo $apex_general_settings['footer_style']; ?>" role="contentinfo">
			<div class="container">
				<div class="row">
					<?php
					$social_links = apex_footer_social_links();
					$copyright = apex_footer_copyright( $social_links );
					if( is_rtl() ) {
						echo $copyright;
						echo $social_links;
					} else {
						echo $social_links;
						echo $copyright;
					}
					?>
				</div><!-- .row -->
				
			</div><!-- .container -->
		</footer><!-- #site-footer -->
		
	</div><!-- #wrapper -->
	
	<a id="apex-totop-float" href="#"><i class="apex-icon-arrow-up"></i></a>
	
<?php wp_footer(); ?>

</body>
</html>